<?php
class Mmd_BrandExample_Helper_Brand extends Mage_Core_Helper_Abstract
{
    public function getBrandUrl(Mmd_BrandDirectory_Model_Brand $brand)
    {
        if (!$brand instanceof Mmd_BrandDirectory_Model_Brand) {
            return '#';
        }
        
        return $this->_getUrl(
            'mmd_brandexample/index/view', 
            array(
                'url' => $brand->getUrlKey(),
            )
        );
    }
}