<?php
class Mmd_BrandExample_Block_List extends Mage_Core_Block_Template
{
    public function getBrandCollection()
    {
        return Mage::getModel('mmd_branddirectory/brand')->getCollection()
            ->addFieldToFilter('visibility', Mmd_BrandDirectory_Model_Brand::VISIBILITY_DIRECTORY)
            ->setOrder('name', 'ASC');
    }
}